# K8s Demo

## Requirements
- vagrant
- ansible

## How to run
### Init environment
```
vagrant up
ansible-playbook -ihosts.yml -uvagrant provision.yml
```

### Config secret for k8s
#### For environment variables
Create file ./k8s/secret.yml from secret.yml.sample.
#### For gitlab container registry
```
vagrant ssh m
kubectl create secret docker-registry regcred --docker-server=<your-registry-server> --docker-username=<your-name> --docker-password=<your-pword> --docker-email=<your-email>
```
### Deploment k8s
```
vagrant ssh m
kubectl apply -f /vagrant/k8s
```
Wait a moment for k8s working  
Access frontend service at http://192.168.50.10:30011

## Reference
- https://kubernetes.io/blog/2019/03/15/kubernetes-setup-using-ansible-and-vagrant/
- https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/
- https://github.com/geerlingguy/ansible-role-kubernetes
- https://github.com/ramitsurana/awesome-kubernetes
- https://kubernetes.io/docs/tasks/access-application-cluster/connecting-frontend-backend/
- https://blog.alexellis.io/kubernetes-in-10-minutes/
